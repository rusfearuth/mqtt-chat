package su.rusfearuth.superchat;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;

/**
 * Created by Alexander on 12/26/2014.
 */
public class MqttRobolectricTestRunner extends RobolectricTestRunner
{
    public MqttRobolectricTestRunner(Class<?> testClass) throws InitializationError
    {
        super(testClass);
    }
}
