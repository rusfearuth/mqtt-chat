package su.rusfearuth.superchat.db;

import com.activeandroid.ActiveAndroid;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import su.rusfearuth.superchat.MqttRobolectricTestRunner;
import su.rusfearuth.superchat.db.helper.ChannelHelper;
import su.rusfearuth.superchat.db.model.Channel;
import su.rusfearuth.superchat.db.model.Message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by rusfearuth on 26.12.14.
 */
@Config(emulateSdk = 16)
@RunWith(MqttRobolectricTestRunner.class)
public class ChannelHelperTest
{	
	private static final String USERNAME = "test_user";
	private static final String CHANNEL_TEST = "/test";
	private static final String CHANNEL_JOKE = "/joke";

	@Before
	public void setUp()
	{
		ActiveAndroid.initialize(Robolectric.application);
	}
	
	@After
	public void tearDown()
	{
		ActiveAndroid.dispose();
	}
	
	@Test
	public void testAddChannel() throws Exception
	{
		Channel channelTest = ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);
		
		assertNotNull("Channel was created", channelTest);
		assertNotSame("Channel id isn't 0", 0l, channelTest.getId().longValue());
	}
	
	@Test
	public void testCount() throws Exception
	{
		assertEquals("Count of empty channel table", 0, ChannelHelper.count());

		ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);
		ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);

		assertEquals("Count of not empty channel table", 1, ChannelHelper.count());
	}
	
	@Test
	public void testCountMessages() throws Exception
	{
		Channel channelTest = ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);
		Channel channelJoke = ChannelHelper.addChannel(CHANNEL_JOKE, USERNAME);
		
		assertEquals("Channel count", 2, ChannelHelper.count());
		
		Message testMessage1 = new Message();
		testMessage1.setChannel(channelTest.getName());
		testMessage1.setName(USERNAME);
		testMessage1.setSent(true);
		testMessage1.setMessage("TEST");
		testMessage1.setUsername(USERNAME);
		testMessage1.setDate(System.currentTimeMillis());
		testMessage1.setForeigner(false);
		testMessage1.save();

		Message testMessage2 = new Message();
		testMessage2.setChannel(channelTest.getName());
		testMessage2.setName(USERNAME);
		testMessage2.setSent(true);
		testMessage2.setMessage("TEST2");
		testMessage2.setUsername(USERNAME);
		testMessage2.setDate(System.currentTimeMillis());
		testMessage2.setForeigner(false);
		testMessage2.save();

		Message testMessage3 = new Message();
		testMessage3.setChannel(channelJoke.getName());
		testMessage3.setName(USERNAME);
		testMessage3.setSent(true);
		testMessage3.setMessage("TEST3");
		testMessage3.setUsername(USERNAME);
		testMessage3.setDate(System.currentTimeMillis());
		testMessage3.setForeigner(false);
		testMessage3.save();
		
		assertEquals("Test channel has", 2, ChannelHelper.countMessages(CHANNEL_TEST, USERNAME));
		assertEquals("Joke channel has", 1, ChannelHelper.countMessages(CHANNEL_JOKE, USERNAME));
	}
	
	@Test
	public void testHasChannel() throws Exception
	{
		ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);
		
		assertTrue("User has /test channel", ChannelHelper.hasChannel(CHANNEL_TEST, USERNAME));
	}

	@Test
	public void testFindChannelByName() throws Exception
	{
		ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);

		assertNotNull("/test channel was found by name and username", ChannelHelper.findChannelByName(CHANNEL_TEST, USERNAME));
	}

	@Test
	public void testFindById() throws Exception
	{
		Channel channelTest = ChannelHelper.addChannel(CHANNEL_TEST, USERNAME);
		Channel found = ChannelHelper.findById(channelTest.getId());

		assertEquals("/test channel was found by id", channelTest.getId(), found.getId());
	}
}
