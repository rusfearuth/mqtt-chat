package su.rusfearuth.superchat.db;

import android.database.Cursor;

import com.activeandroid.ActiveAndroid;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;

import su.rusfearuth.superchat.MqttRobolectricTestRunner;
import su.rusfearuth.superchat.db.helper.MessageHelper;
import su.rusfearuth.superchat.db.model.Message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by rusfearuth on 27.12.14.
 */
@Config(emulateSdk = 16)
@RunWith(MqttRobolectricTestRunner.class)
public class MessageHelperTest
{
	private final static String CHANNEL_TEST = "/test";
	private final static String CHANNEL_JOKE = "/joke";
	private final static String USERNAME = "vasya_pupok";
	private final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
	
	
	@Before
	public void setUp()
	{
		ActiveAndroid.initialize(Robolectric.application);
	}
	
	@After
	public void tearDown()
	{
		ActiveAndroid.dispose();
	}
	
	@Test
	public void testAddMessage() throws Exception
	{
		JSONObject msg = new JSONObject();
		msg.put("text", "message text here");
		msg.put("username", "vasay_pupok");
		msg.put("clientId", "0123456789");
		msg.put("date", formatter.format(System.currentTimeMillis()));
		
		Message message = MessageHelper.addNewMessage(msg, CHANNEL_TEST, USERNAME);
		
		assertNotNull("Message was created", message);
	}
	
	@Test
	public void testCount() throws Exception
	{
		assertEquals("Message table is empty", 0, MessageHelper.count());

		JSONObject msg = new JSONObject();
		msg.put("text", "message text here");
		msg.put("username", "vasay_pupok");
		msg.put("clientId", "0123456789");
		msg.put("date", formatter.format(System.currentTimeMillis()));

		MessageHelper.addNewMessage(msg, CHANNEL_TEST, USERNAME);
		
		assertEquals("Message table isn't empty", 1, MessageHelper.count());
	}

	@Test
	public void testChannelMessage() throws Exception
	{
		assertEquals("Message table is empty", 0, MessageHelper.count());

		JSONObject msg = new JSONObject();
		msg.put("text", "message text here");
		msg.put("username", "vasay_pupok");
		msg.put("clientId", "0123456789");
		msg.put("date", formatter.format(System.currentTimeMillis()));

		MessageHelper.addNewMessage(msg, CHANNEL_TEST, USERNAME);

		JSONObject msg2 = new JSONObject();
		msg2.put("text", "It's a joke");
		msg2.put("username", "vasay_pupok");
		msg2.put("clientId", "0123456789");
		msg2.put("date", formatter.format(System.currentTimeMillis()));
		
		MessageHelper.addNewMessage(msg2, CHANNEL_TEST, USERNAME);

		Cursor channelTestCursor = MessageHelper.channelMessage(CHANNEL_TEST, USERNAME);
		int channeTestCount = channelTestCursor.getCount();
		channelTestCursor.close();
		
		Cursor channelJokeCursor = MessageHelper.channelMessage(CHANNEL_JOKE, USERNAME);
		int channelJokeCount = channelJokeCursor.getCount();
		channelJokeCursor.close();

		assertEquals("/test channel has", 2, channeTestCount);
		assertEquals("/joke channel has", 0, channelJokeCount);
	}

	@Test
	public void testFindById() throws Exception
	{
		JSONObject msg = new JSONObject();
		msg.put("text", "message text here");
		msg.put("username", "vasay_pupok");
		msg.put("clientId", "0123456789");
		msg.put("date", formatter.format(System.currentTimeMillis()));

		Message message = MessageHelper.addNewMessage(msg, CHANNEL_TEST, USERNAME);
		Message found = MessageHelper.findById(message.getId());
		
		assertNotNull("Message was found", found);
		assertEquals("Messages are equal", message.getId(), found.getId());
	}
}
