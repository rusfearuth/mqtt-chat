package su.rusfearuth.superchat.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;

import su.rusfearuth.superchat.MqttRobolectricTestRunner;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.ui.about.fragment.AboutFragment;
import su.rusfearuth.superchat.ui.base.BaseActivity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Alexander on 12/26/2014.
 */
@Config(emulateSdk = 16)
@RunWith(MqttRobolectricTestRunner.class)
public class FragmentUtilsTest
{
    @Test
    public void testFragmentUtils_addFragment() throws Exception
    {
        BaseActivity activity = Robolectric.buildActivity(BaseActivity.class)
                .create().start().restart().get();
        FragmentUtils.addFragment(activity, R.id.content, AboutFragment.class, null, false);

        FragmentManager manager = activity.getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.content);
        assertNotNull("Fragment was attached", fragment);
        assertEquals("Fragment is AboutFragment", fragment.getClass(), AboutFragment.class);
    }
}
