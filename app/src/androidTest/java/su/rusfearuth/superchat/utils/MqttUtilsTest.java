package su.rusfearuth.superchat.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import su.rusfearuth.superchat.MqttRobolectricTestRunner;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by Alexander on 12/26/2014.
 */
@Config(emulateSdk = 16)
@RunWith(MqttRobolectricTestRunner.class)
public class MqttUtilsTest
{
    private static final String OK_RESULT = "/test";
    private static final Long MIN_VALUE_CLIENT_ID = 666666667l;
    private static final Long MAX_VALUE_CLIENT_ID = 999999998l;

    @Test
    public void testMqttUtils_fixChannelName() throws Exception
    {
        String newChannelName = MqttUtils.fixChannelName("test");
        assertEquals("Test channel name without /", OK_RESULT, newChannelName);

        String newCorrectChannelName = MqttUtils.fixChannelName("/test");
        assertEquals("Test channel name with /", OK_RESULT, newCorrectChannelName);
    }

    @Test
    public void testMqttUtils_generateClientId() throws Exception
    {
        Long newClientId = Long.valueOf(MqttUtils.generateClientId());
        assertTrue("Higher or equal MIN VALUE", newClientId >= MIN_VALUE_CLIENT_ID);
        assertTrue("Lower or equal MAX VALUE", newClientId <= MAX_VALUE_CLIENT_ID);
    }
}
