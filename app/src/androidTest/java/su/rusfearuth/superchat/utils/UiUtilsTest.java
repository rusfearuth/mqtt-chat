package su.rusfearuth.superchat.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowInputMethodManager;

import su.rusfearuth.superchat.MqttRobolectricTestRunner;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Alexander on 12/26/2014.
 */
@Config(emulateSdk = 16)
@RunWith(MqttRobolectricTestRunner.class)
public class UiUtilsTest
{
    @Test
    public void testUiUtils_hideSoftwareKeyboard() throws Exception
    {
        InputMethodManager im = (InputMethodManager) Robolectric.application
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        ShadowInputMethodManager sim = Robolectric.shadowOf(im);


        assertTrue("Software keyboard is invisible [first state]", !sim.isSoftInputVisible());

        sim.showSoftInput(null, 0);

        assertTrue("Software keyboard is visible [called by user]", sim.isSoftInputVisible());

        UiUtils.hideSoftwareKeyboard(Robolectric.application);

        assertTrue("Software keyboard is invisible [hidden by ui utils]", !sim.isSoftInputVisible());
    }
}
