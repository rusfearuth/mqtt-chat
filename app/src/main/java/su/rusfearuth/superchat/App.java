package su.rusfearuth.superchat;

import android.app.Application;
import android.content.Intent;

import com.activeandroid.ActiveAndroid;

import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.service.ChatService;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class App extends Application
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		initAppEvents();
		ActiveAndroid.initialize(this);
		TypefaceUtils.setDefaultFont("Lora-Regular.ttf");
		SuperChatManager.getInstance().init(this);
		startService(new Intent(this, ChatService.class));
	}

	public void initAppEvents()
	{
		EventManager.getInstance().init(new EventManager.InitListener()
		{
			@Override
			public void onInit()
			{
				EventManager.getInstance().register(AppEvent.Interface.SELECT_CHANNEL, PublishSubject.create());

				EventManager.getInstance().register(AppEvent.Server.OPEN_CONNECTION, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.CONNECTION_OPENED, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.CONNECTION_FAILED, BehaviorSubject.create());
				EventManager.getInstance().register(AppEvent.Server.SUBSCRIPTION_FAILED, BehaviorSubject.create());
				EventManager.getInstance().register(AppEvent.Server.CLOSE_CONNECTION, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.MESSAGE_ARRIVED, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.NOTIFY_ABOUT_MESSAGE, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.SUBSCRIBE_CHANNEL, BehaviorSubject.create());
				EventManager.getInstance().register(AppEvent.Server.UNSUBSCRIBE_CHANNEL, BehaviorSubject.create());
				EventManager.getInstance().register(AppEvent.Server.SEND_MESSAGE, PublishSubject.create());
				EventManager.getInstance().register(AppEvent.Server.SEND_TYPING, PublishSubject.create());
			}
		});
	}
}
