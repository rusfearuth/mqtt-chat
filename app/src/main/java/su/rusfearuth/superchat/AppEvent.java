package su.rusfearuth.superchat;

/**
 * Created by rusfearuth on 19.12.14.
 */
public interface AppEvent
{
	public static final String PREFIX = AppEvent.class.getCanonicalName();

	public interface Interface
	{
		public static final String SELECT_CHANNEL = new StringBuilder(PREFIX)
				.append(".interface.select.chanel").toString();
	}

	public interface Server
	{
		public static final String OPEN_CONNECTION = new StringBuilder(PREFIX)
				.append(".server.open.connection").toString();
		public static final String CONNECTION_OPENED = new StringBuilder(PREFIX)
				.append(".server.connection.opened").toString();
		public static final String CONNECTION_FAILED = new StringBuilder(PREFIX)
				.append(".server.connection.failed").toString();
		public static final String SUBSCRIPTION_FAILED = new StringBuilder(PREFIX)
				.append(".server.subscription.failed").toString();
		public static final String CLOSE_CONNECTION = new StringBuilder(PREFIX)
				.append(".server.close.connection").toString();

		public static final String MESSAGE_ARRIVED = new StringBuilder(PREFIX)
				.append(".server.message.arrived").toString();
		public static final String NOTIFY_ABOUT_MESSAGE = new StringBuilder(PREFIX)
				.append(".server.notity.about.message").toString();

		public static final String SUBSCRIBE_CHANNEL = new StringBuilder(PREFIX)
				.append(".server.subscribe.channel").toString();
		public static final String UNSUBSCRIBE_CHANNEL = new StringBuilder(PREFIX)
				.append(".server.unsubscribe.channel").toString();
		public static final String SEND_MESSAGE = new StringBuilder(PREFIX)
				.append(".server.send.message").toString();
		public static final String SEND_TYPING = new StringBuilder(PREFIX)
				.append(".server.send.typing").toString();
	}
}
