package su.rusfearuth.superchat.context;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import su.rusfearuth.superchat.db.model.Message;

/**
 * Created by rusfearuth on 21.12.14.
 */
public class Connection
{
	private String clientId;
	private String address;
	private int port = 1883;
	private MemoryPersistence persistence = new MemoryPersistence();
	private MqttAndroidClient client;
	private int qos = 2;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

	public Connection()
	{}

	public void connect(@NonNull Context context,
	                                 MqttCallback callback,
	                                 IMqttActionListener actionListener) throws MqttException
	{
		Log.e("CONNECTION ADDRESS", toConnectionAddress());
		client = new MqttAndroidClient(context, toConnectionAddress(), clientId, new MemoryPersistence());
		client.setCallback(callback);
		client.connect(null, actionListener);
	}

	public void subscribe(@NonNull String topic) throws MqttException
	{
		topic = topicCheckout(topic);

		if (client != null)
			client.subscribe(topic, qos);
	}

	public void unsubscribe(@NonNull String topic) throws MqttException
	{
		topic = topicCheckout(topic);

		if (client != null)
			client.unsubscribe(topic);
	}

	public void sendMessage(@NonNull Message message)
	{
		MqttMessage mqttMessage = new MqttMessage(buildMessageFormat(message).getBytes());
		mqttMessage.setQos(qos);
		if (client != null && client.isConnected())
		{
			try
			{
				IMqttDeliveryToken token = client.publish(message.getChannel(), mqttMessage);
				Log.e("SEND MESSAGE", String.valueOf(token.getMessageId()));
			}
			catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void sendTyping(@NonNull String topic, @NonNull String username)
	{
		topic = topicCheckout(topic);
		MqttMessage mqttMessage = new MqttMessage(buildMessageForTyping(username).getBytes());
		mqttMessage.setQos(0);
		if (client != null && client.isConnected())
		{
			try
			{
				client.publish(topic, mqttMessage);
			}
			catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	}

	public String getClientId()
	{
		return clientId;
	}

	public void setClientId(String clientId)
	{
		this.clientId = clientId;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public int getPort()
	{
		return port;
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public String toConnectionAddress()
	{
		return new StringBuilder("tcp://")
				.append(getAddress())
				.append(":").append(getPort()).toString();
	}

	public void close()
	{
		try
		{
			if (client != null && client.isConnected())
			{
				try
				{
					client.disconnect();
				}
				catch (MqttException e)
				{
					e.printStackTrace();
				}
				client = null;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			client = null;
		}

	}

	public boolean isActive()
	{
		return client != null && client.isConnected();
	}

	private String topicCheckout(String topic)
	{
		if (!topic.startsWith("/"))
			topic = new StringBuilder("/").append(topic).toString();
		return topic;
	}

	private String buildMessageFormat(@NonNull Message message)
	{
		JSONObject jsonMessage = new JSONObject();
		try
		{
			jsonMessage.put("type", "message");
			jsonMessage.put("text", message.getMessage());
			jsonMessage.put("username", message.getName());
			jsonMessage.put("clientId", clientId);
			jsonMessage.put("date", dateFormat.format(message.getDate()));
			jsonMessage.put("_lid", message.getId());
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		return jsonMessage.toString();
	}

	private String buildMessageForTyping(@NonNull String userName)
	{
		JSONObject jsonMessage = new JSONObject();
		try
		{
			jsonMessage.put("type", "typing");
			jsonMessage.put("username", userName);
			jsonMessage.put("clientId", clientId);
			jsonMessage.put("date", dateFormat.format(System.currentTimeMillis()));
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		return jsonMessage.toString();
	}
}
