package su.rusfearuth.superchat.context;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by rusfearuth on 20.12.14.
 */
public class SuperChatManager
{
	private static final String LOG_TAG = SuperChatManager.class.getCanonicalName();

	private interface Params
	{
		public static final String SHARED_PREF = new StringBuilder(LOG_TAG)
				.append(".params.shared.pref").toString();

		public interface Common
		{
			public static final String USER_NAME = new StringBuilder(LOG_TAG)
					.append(".params.common.user.name").toString();

		}

		public interface Connection
		{
			public static final String CLIENT_ID = new StringBuilder(LOG_TAG)
					.append(".params.connection.client.id").toString();
			public static final String ADDRESS = new StringBuilder(LOG_TAG)
					.append(".params.connection.address").toString();
			public static final String PORT = new StringBuilder(LOG_TAG)
					.append(".params.connection.port").toString();
		}
	}

	private static volatile SuperChatManager instance;
	private Context context;

	private String name;
	private String clientId;
	private String serverAddress;
	private int serverPort = 1883;
	private Set<String> subscription = new LinkedHashSet<>();

	public static SuperChatManager getInstance()
	{
		if (instance == null)
		{
			synchronized (SuperChatManager.class)
			{
				if (instance == null)
					instance = new SuperChatManager();
			}
		}
		return instance;
	}

	public void init(Context context)
	{
		this.context = context;
		loadData();
	}

	public boolean isAuth()
	{
		return !TextUtils.isEmpty(clientId) && !TextUtils.isEmpty(serverAddress) &&
				!TextUtils.isEmpty(name);
	}

	public void setName(@NonNull String name)
	{
		this.name = name;
		saveData();
	}

	public String getName()
	{
		return name;
	}

	public String getServerAddress()
	{
		return serverAddress;
	}

	public void setServerAddress(String serverAddress)
	{
		this.serverAddress = serverAddress;
		saveData();
	}

	public int getServerPort()
	{
		return serverPort;
	}

	public void setServerPort(int serverPort)
	{
		this.serverPort = serverPort;
		saveData();
	}

	public void setServerClientId(String clientId)
	{
		this.clientId = clientId;
		saveData();
	}

	public String getServerClientId()
	{
		return this.clientId;
	}

	public void dropServerSettings()
	{
		clientId = null;
		serverAddress = null;
		serverPort = 0;
		saveData();
	}

	public void addSubscribe(@NonNull String channel)
	{
		subscription.add(channel);
	}

	public void removeSubscribe(@NonNull String channel)
	{
		if (subscription.contains(channel))
			subscription.remove(channel);
	}

	public Set<String> getSubscription()
	{
		return subscription;
	}

	private void loadData()
	{
		SharedPreferences pref = context
				.getSharedPreferences(Params.SHARED_PREF, Context.MODE_PRIVATE);

		name = pref.getString(Params.Common.USER_NAME, null);

		clientId = pref.getString(Params.Connection.CLIENT_ID, null);
		serverAddress = pref.getString(Params.Connection.ADDRESS, null);
		serverPort = pref.getInt(Params.Connection.PORT, 1883);
	}

	private void saveData()
	{
		SharedPreferences pref = context
				.getSharedPreferences(Params.SHARED_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();

		editor.putString(Params.Common.USER_NAME, name);
		editor.putString(Params.Connection.CLIENT_ID, clientId);
		editor.putString(Params.Connection.ADDRESS, serverAddress);
		editor.putInt(Params.Connection.PORT, serverPort);
		editor.commit();
	}

	private SuperChatManager()
	{}
}
