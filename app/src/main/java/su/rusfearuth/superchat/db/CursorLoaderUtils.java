package su.rusfearuth.superchat.db;

import android.database.Cursor;
import android.support.annotation.NonNull;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.db.helper.ChannelHelper;
import su.rusfearuth.superchat.db.helper.MessageHelper;
import su.rusfearuth.superchat.utils.MqttUtils;

/**
 * Created by rusfearuth on 20.12.14.
 */
public class CursorLoaderUtils
{
	public static Observable<Cursor> loadChannels()
	{
		final String username = SuperChatManager.getInstance().getName();
		Observable<Cursor> observable = Observable.create(new Observable.OnSubscribe<Cursor>()
		{
			@Override
			public void call(Subscriber<? super Cursor> subscriber)
			{
				subscriber.onNext(ChannelHelper.cursor(username));
				subscriber.onCompleted();
			}
		});

		return addExtras(observable);
	}

	public static Observable<Cursor> loadMessage(@NonNull final String channel)
	{
		final String username = SuperChatManager.getInstance().getName();
		Observable<Cursor> observable = Observable.create(new Observable.OnSubscribe<Cursor>()
		{
			@Override
			public void call(Subscriber<? super Cursor> subscriber)
			{
				subscriber.onNext(MessageHelper.channelMessage(MqttUtils.fixChannelName(channel), username));
				subscriber.onCompleted();
			}
		});
		return addExtras(observable);
	}

	private static Observable addExtras(Observable observable)
	{
		observable
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());
		return observable;
	}
}
