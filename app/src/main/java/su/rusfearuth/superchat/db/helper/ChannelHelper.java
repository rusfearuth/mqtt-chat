package su.rusfearuth.superchat.db.helper;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import su.rusfearuth.superchat.db.model.Channel;
import su.rusfearuth.superchat.db.model.Message;
import su.rusfearuth.superchat.db.spec.MessageSpec;
import su.rusfearuth.superchat.utils.MqttUtils;

import static su.rusfearuth.superchat.db.spec.ChannelSpec.Column;

/**
 * Created by rusfearuth on 20.12.14.
 */
public class ChannelHelper
{
	public static int count()
	{
		return new Select()
				.from(Channel.class)
				.count();
	}

	public static int countMessages(@NonNull String channel, @NonNull String username)
	{
		return new Select()
				.from(Message.class)
				.where(new StringBuilder(MessageSpec.Column.CHANNEL)
						.append(" = ?").toString(), MqttUtils.fixChannelName(channel))
				.and(new StringBuilder(MessageSpec.Column.USERNAME)
						.append(" = ?").toString(), username)
				.count();
	}

	public static Channel findById(@NonNull Long id)
	{
		return Channel.load(Channel.class, id);
	}

	public static Channel findChannelByName(@NonNull String name, @NonNull String username)
	{
		return new Select()
				.from(Channel.class)
				.where(new StringBuilder(Column.NAME).append(" = ?").toString(), name)
				.and(new StringBuilder(Column.USERNAME).append(" = ?").toString(), username)
				.executeSingle();
	}

	public static boolean hasChannel(@NonNull String name, @NonNull String username)
	{
		boolean result;

		Channel channel = findChannelByName(name, username);

		result = channel != null;

		return result;
	}

	public static Channel addChannel(@NonNull String name, @NonNull String username)
	{
		Channel result = findChannelByName(name, username);
		if (result == null)
		{
			result = new Channel();
			result.setName(name);
			result.setUsername(username);
			result.save();
		}
		else
		{
			result.setUsername(username);
			result.save();
		}
		return result;
	}

	public static Cursor cursor(@NonNull String username)
	{
		String sql = new Select()
				.from(Channel.class)
				.where(new StringBuilder(Column.USERNAME).append(" = ?").toString())
				.orderBy(new StringBuilder(Column.NAME).append(" DESC").toString())
				.toSql();
		return ActiveAndroid.getDatabase().rawQuery(sql, new String[] { username });
	}
}
