package su.rusfearuth.superchat.db.helper;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import su.rusfearuth.superchat.db.model.Message;
import su.rusfearuth.superchat.db.spec.MessageSpec;
import su.rusfearuth.superchat.utils.MqttUtils;

/**
 * Created by rusfearuth on 21.12.14.
 */
public class MessageHelper
{
	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

	public static int count()
	{
		return new Select()
				.from(Message.class)
				.count();
	}

	public static Message findById(Long id)
	{
		return Message.load(Message.class, id);
	}

	public static Message addNewMessage(@NonNull JSONObject object, @NonNull String channel,
	                                    @NonNull String username)
	{
		Message result = null;
		if (object.has("text") && object.has("username") && object.has("clientId")
				&& object.has("date"))
		{
			result = new Message();
			result.setName(object.optString("username"));
			result.setMessage(object.optString("text"));
			result.setChannel(MqttUtils.fixChannelName(channel));
			result.setUsername(username);
			try
			{
				Date date = format.parse(object.optString("date"));
				result.setDate(date.getTime());
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			result.setForeigner(true);
			result.setSent(true);
			result.save();
		}
		return result;
	}

	public static Cursor channelMessage(@NonNull String channel, @NonNull String username)
	{
		String selection = new StringBuilder(MessageSpec.Column.CHANNEL)
				.append(" = ? and ").append(MessageSpec.Column.USERNAME)
				.append(" = ?").toString();
		String sql = new Select()
				.from(Message.class)
				.where(selection)
				.toSql();
		return ActiveAndroid.getDatabase().rawQuery(sql, new String[] { channel, username });
	}
}
