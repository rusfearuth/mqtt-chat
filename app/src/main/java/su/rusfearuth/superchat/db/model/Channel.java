package su.rusfearuth.superchat.db.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import su.rusfearuth.superchat.db.spec.ChannelSpec;

import static su.rusfearuth.superchat.db.spec.ChannelSpec.TABLE;

/**
 * Created by rusfearuth on 20.12.14.
 */
@Table(name = TABLE, id = BaseColumns._ID)
public class Channel extends Model
{
	@Column(name = ChannelSpec.Column.NAME, notNull = true, index = true)
	private String name;
	@Column(name = ChannelSpec.Column.USERNAME, notNull = true, index = true)
	private String username;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}
}
