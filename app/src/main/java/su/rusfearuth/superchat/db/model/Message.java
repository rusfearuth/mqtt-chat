package su.rusfearuth.superchat.db.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import su.rusfearuth.superchat.db.spec.MessageSpec;

/**
 * Created by rusfearuth on 21.12.14.
 */
@Table(name = MessageSpec.TABLE, id = BaseColumns._ID)
public class Message extends Model
{
	@Column(name = MessageSpec.Column.NAME, notNull = true)
	private String name;
	@Column(name = MessageSpec.Column.MESSAGE)
	private String message;
	@Column(name = MessageSpec.Column.CHANNEL, notNull = true, index = true)
	private String channel;
	@Column(name = MessageSpec.Column.DATE, notNull = true)
	private Long date;
	@Column(name = MessageSpec.Column.FOREIGNER, notNull = true)
	private Boolean foreigner;
	@Column(name = MessageSpec.Column.IS_SENT, notNull = true)
	private Boolean sent = Boolean.FALSE;
	@Column(name = MessageSpec.Column.USERNAME, notNull = true, index = true)
	private String username;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Long getDate()
	{
		return date;
	}

	public void setDate(Long date)
	{
		this.date = date;
	}

	public Boolean getForeigner()
	{
		return foreigner;
	}

	public void setForeigner(Boolean foreigner)
	{
		this.foreigner = foreigner;
	}

	public String getChannel()
	{
		return channel;
	}

	public void setChannel(String channel)
	{
		this.channel = channel;
	}

	public Boolean getSent()
	{
		return sent;
	}

	public void setSent(Boolean sent)
	{
		this.sent = sent;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}
}
