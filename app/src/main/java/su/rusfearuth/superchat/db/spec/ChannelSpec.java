package su.rusfearuth.superchat.db.spec;

/**
 * Created by rusfearuth on 20.12.14.
 */
public interface ChannelSpec
{
	public static final String TABLE = "superchat_channel";

	public interface Column
	{
		public static final String NAME = "name";
		public static final String USERNAME = "username";
	}
}
