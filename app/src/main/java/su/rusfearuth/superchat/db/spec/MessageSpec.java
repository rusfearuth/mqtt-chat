package su.rusfearuth.superchat.db.spec;

/**
 * Created by rusfearuth on 21.12.14.
 */
public interface MessageSpec
{
	public static final String TABLE = "superchat_message";

	public interface Column
	{
		public static final String NAME = "name";
		public static final String MESSAGE = "message";
		public static final String DATE = "came_at";
		public static final String CHANNEL = "channel";
		public static final String IS_SENT = "is_sent";
		public static final String FOREIGNER = "foreigner";
		public static final String USERNAME = "username";
	}
}
