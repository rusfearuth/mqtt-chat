package su.rusfearuth.superchat.event;

import android.support.annotation.NonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import rx.subjects.Subject;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class EventManager
{
	private static volatile EventManager instance;

	public interface InitListener
	{
		void onInit();
	}

	private Map<String, Subject> subjects = new ConcurrentHashMap<>();


	public static EventManager getInstance()
	{
		if (instance == null)
		{
			synchronized (EventManager.class)
			{
				if (instance == null)
					instance = new EventManager();
			}
		}
		return instance;
	}

	public void init(InitListener initListener)
	{
		if (initListener != null)
		{
			initListener.onInit();
		}
	}

	public void register(@NonNull String key, @NonNull Subject subject)
	{
		if (!subjects.containsKey(key))
			subjects.put(key, subject);
	}

	public void unregister(@NonNull String key)
	{
		if (subjects.containsKey(key))
		{
			subjects.remove(key);
		}
	}

	public Subject watch(@NonNull String key)
	{
		Subject result = null;
		if (subjects.containsKey(key))
			result = subjects.get(key);
		return result;
	}

	public <T> void push(@NonNull String key, T object)
	{
		if (subjects.containsKey(key))
			subjects.get(key).onNext(object);
	}

	private EventManager()
	{}
}
