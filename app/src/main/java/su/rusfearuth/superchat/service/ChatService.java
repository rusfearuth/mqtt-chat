package su.rusfearuth.superchat.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.net.ConnectivityManagerCompat;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

import rx.functions.Action1;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.context.Connection;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.db.model.Message;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.ui.main.MainActivity;
import su.rusfearuth.superchat.utils.MqttUtils;

/**
 * Created by rusfearuth on 21.12.14.
 */
public class ChatService extends Service
	implements IMqttActionListener, MqttCallback
{
	private AtomicBoolean closeConnection = new AtomicBoolean(false);
	private AtomicBoolean broadcastRegistered = new AtomicBoolean(false);

	private BroadcastReceiver onChangeNetworkState = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Log.e("NETWORK CHANGED", String.valueOf(intent.getAction()));
			NetworkInfo networkInfo = ConnectivityManagerCompat
					.getNetworkInfoFromBroadcast(connectivityManager, intent);
			if (networkInfo != null && networkInfo.isAvailable() &&
					networkInfo.isConnected() && client == null)
			{
				if (getSuperChatManager().isAuth())
					getEventManager().push(AppEvent.Server.OPEN_CONNECTION, null);
			}
			Log.e("COMPAT STATE", String.valueOf(networkInfo));
		}
	};

	private Action1 onPullNotification = new Action1<Message>()
	{
		@Override
		public void call(Message msg)
		{
			Intent intent;
			Intent compatIntent = new Intent(getApplication(), MainActivity.class);
			intent = IntentCompat.makeRestartActivityTask(compatIntent.getComponent());

			PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
					intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

			NotificationCompat.Builder mBuilder =
					new NotificationCompat.Builder(getApplication())
							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle(msg.getName())
							.setStyle(new NotificationCompat.BigTextStyle()
									.bigText(msg.getMessage()))
							.setContentText(msg.getMessage())
							.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
							.setAutoCancel(true);

			mBuilder.setContentIntent(contentIntent);
			notificationManager.notify(1, mBuilder.build());
		}
	};

	private Action1 onOpenNewConnection = new Action1()
	{
		@Override
		public void call(Object o)
		{
			if (client != null)
				client.close();
			if (getSuperChatManager().isAuth())
			{
				openConnection();
			}
		}
	};

	private Action1 onOpenConnection = new Action1()
	{
		@Override
		public void call(Object o)
		{
			if (client == null)
				return;
			shutdownConnectionWatcher();
			getSuperChatManager().setServerClientId(client.getClientId());
			getSuperChatManager().setServerAddress(client.getAddress());
			getSuperChatManager().setServerPort(client.getPort());
			for (String channel: getSuperChatManager().getSubscription())
			{
				getEventManager().push(AppEvent.Server.SUBSCRIBE_CHANNEL, channel);
			}
		}
	};

	private Action1 onCloseConnection = new Action1()
	{
		@Override
		public void call(Object o)
		{
			closeConnection.set(true);
			if (client == null)
				return;
			client.close();
			client = null;
		}
	};

	private Action1 onFailedConnection = new Action1<Throwable>()
	{
		@Override
		public void call(Throwable o)
		{
			getSuperChatManager().dropServerSettings();
			client = null;
		}
	};

	private Action1 onSubscribeChannel = new Action1<String>()
	{
		@Override
		public void call(String channel)
		{
			if (client == null)
				return;
			try
			{
				client.subscribe(channel);
				getSuperChatManager().addSubscribe(channel);
			}
			catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	};

	private Action1 onUnsubscribeChannel = new Action1<String>()
	{
		@Override
		public void call(String channel)
		{
			if (client == null)
				return;
			try
			{
				client.unsubscribe(channel);
				getSuperChatManager().removeSubscribe(channel);
			}
			catch (MqttException e)
			{
				e.printStackTrace();
			}
		}
	};


	private Action1 onSendMessage = new Action1<Message>()
	{
		@Override
		public void call(Message message)
		{
			if (client == null)
				return;
			client.sendMessage(message);
		}
	};

	private Action1 onSendTyping = new Action1<String>()
	{
		@Override
		public void call(String channel)
		{
			if (client == null)
				return;
			client.sendTyping(channel, getSuperChatManager().getName());
		}
	};

	private Connection client;
	private ConnectivityManager connectivityManager;
	private NotificationManager notificationManager;

	@Override
	public void onCreate()
	{
		super.onCreate();

		connectivityManager = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		notificationManager = (NotificationManager) getApplicationContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		getEventManager().watch(AppEvent.Server.OPEN_CONNECTION)
				.subscribe(onOpenNewConnection);
		getEventManager().watch(AppEvent.Server.CONNECTION_OPENED)
				.subscribe(onOpenConnection);
		getEventManager().watch(AppEvent.Server.CLOSE_CONNECTION)
				.subscribe(onCloseConnection);
		getEventManager().watch(AppEvent.Server.CONNECTION_FAILED)
				.subscribe(onFailedConnection);
		getEventManager().watch(AppEvent.Server.SUBSCRIPTION_FAILED)
				.subscribe();
		getEventManager().watch(AppEvent.Server.SUBSCRIBE_CHANNEL)
				.subscribe(onSubscribeChannel);
		getEventManager().watch(AppEvent.Server.UNSUBSCRIBE_CHANNEL)
				.subscribe(onUnsubscribeChannel);
		getEventManager().watch(AppEvent.Server.SEND_MESSAGE)
				.subscribe(onSendMessage);
		getEventManager().watch(AppEvent.Server.NOTIFY_ABOUT_MESSAGE)
				.subscribe(onPullNotification);
		getEventManager().watch(AppEvent.Server.SEND_TYPING)
				.subscribe(onSendTyping);

	}


	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public void onSuccess(IMqttToken iMqttToken)
	{
		getEventManager().push(AppEvent.Server.CONNECTION_OPENED, null);
	}

	@Override
	public void onFailure(IMqttToken iMqttToken, Throwable throwable)
	{
		getEventManager().push(AppEvent.Server.CONNECTION_FAILED, throwable);
		Log.e("CONNECTION FAILER", String.valueOf(throwable));
	}

	@Override
	public void connectionLost(Throwable throwable)
	{
		Log.e("MQTT connection lost", String.valueOf(throwable));
		if (closeConnection.get())
			return;

		if (client != null)
			client.close();
		client = null;
		wakeupConnectionWatcher();
	}

	@Override
	public void messageArrived(String s, MqttMessage mqttMessage) throws Exception
	{
		Log.d("MQTT", "**********Topic: **********");
		Log.d("MQTT", "Message: "+mqttMessage.toString());
		Log.d("MQTT", "QoS: "+mqttMessage.getQos());
		Log.d("MQTT", "payload: "+mqttMessage.getPayload());
		Log.d("MQTT", "isDuplicate? "+mqttMessage.isDuplicate());
		Log.d("MQTT", "isRetained?"+mqttMessage.isRetained());
		Log.e("MQTT message arrived", String.valueOf(s) + " / " + String.valueOf(mqttMessage) + " / ");
		try
		{
			JSONObject msg = new JSONObject(String.valueOf(mqttMessage));
			switch (msg.optString("type"))
			{
				case "message":
					MqttUtils.processMessage(s, msg, client.getClientId());
					break;
			}


		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken)
	{
		Log.e("MQTT delivery complete", String.valueOf(iMqttDeliveryToken));
	}

	private void wakeupConnectionWatcher()
	{
		if (!broadcastRegistered.get())
		{
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
			intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
			registerReceiver(onChangeNetworkState, intentFilter);
		}
	}

	private void shutdownConnectionWatcher()
	{
		if (broadcastRegistered.get())
			unregisterReceiver(onChangeNetworkState);
	}

	private void openConnection()
	{
		if (client == null)
		{
			client = new Connection();
			client.setClientId(getSuperChatManager().getServerClientId());
			client.setAddress(getSuperChatManager().getServerAddress());
			if (getSuperChatManager().getServerPort() != 0)
				client.setPort(getSuperChatManager().getServerPort());

		}
		try
		{
			client.connect(getApplicationContext(), this, this);
			closeConnection.set(false);
		}
		catch (MqttException e)
		{
			e.printStackTrace();
			EventManager.getInstance().push(AppEvent.Server.CONNECTION_FAILED, e);
		}
	}

	private EventManager getEventManager()
	{
		return EventManager.getInstance();
	}

	private SuperChatManager getSuperChatManager()
	{
		return SuperChatManager.getInstance();
	}



}
