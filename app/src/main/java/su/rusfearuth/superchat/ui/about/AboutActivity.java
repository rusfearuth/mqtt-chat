package su.rusfearuth.superchat.ui.about;

import android.os.Bundle;

import su.rusfearuth.superchat.ui.about.fragment.AboutFragment;
import su.rusfearuth.superchat.ui.base.BaseDrawerActivity;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class AboutActivity extends BaseDrawerActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addContentFragment(AboutFragment.class, null, false);
	}
}
