package su.rusfearuth.superchat.ui.about.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.ui.base.BaseFragment;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class AboutFragment extends BaseFragment
{
	@Override
	public int getLayoutId()
	{
		return R.layout.fragment_about;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		ImageView avatar = (ImageView) view.findViewById(R.id.imgAvatar);
		if (isAdded())
			Picasso.with(getActivity())
				.load("https://lh6.googleusercontent.com/-ukHAyqL_pAc/AAAAAAAAAAI/AAAAAAAACZQ/p_QBKWrIDm0/s160-c/photo.jpg")
				.into(avatar);
		if (isAdded())
			TypefaceUtils.invalidateFonts(getActivity(), view);
		super.onViewCreated(view, savedInstanceState);
	}
}
