package su.rusfearuth.superchat.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.utils.FragmentUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class BaseActivity extends FragmentActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
	}

	public void addContentFragment(Class<? extends Fragment> clazz, Bundle args, boolean toStack)
	{
		FragmentUtils.addFragment(this, R.id.content, clazz, args, toStack);
	}

	public EventManager getEventManager()
	{
		return EventManager.getInstance();
	}

	public SuperChatManager getSuperChatManager()
	{
		return SuperChatManager.getInstance();
	}
}
