package su.rusfearuth.superchat.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.utils.FragmentUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class BaseBackActivity extends ActionBarActivity
{
	private Toolbar toolbar;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base_back);
		onInitToolbar();
	}

	public void onInitToolbar()
	{
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		toolbar.setTitle(getTitle());
	}

	public void addContentFragment(Class<? extends Fragment> clazz, Bundle args, boolean toStack)
	{
		FragmentUtils.addFragment(this, R.id.content, clazz, args, toStack);
	}

	public EventManager getEventManager()
	{
		return EventManager.getInstance();
	}
}
