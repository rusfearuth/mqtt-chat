package su.rusfearuth.superchat.ui.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.utils.FragmentUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class BaseDrawerActivity extends ActionBarActivity
{
	private ActionBarDrawerToggle btnDrawerToggle;
	private Toolbar toolbar;
	private DrawerLayout drawerLayout;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base_drawer);
		onInitToolbar();
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		btnDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
		drawerLayout.setDrawerListener(btnDrawerToggle);
	}

	@Override
	public void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		btnDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		btnDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (btnDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onInitToolbar()
	{
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public void addContentFragment(Class<? extends Fragment> clazz, Bundle args, boolean toStack)
	{
		FragmentUtils.addFragment(this, R.id.content, clazz, args, toStack);
	}

	public EventManager getEventManager()
	{
		return EventManager.getInstance();
	}

	protected DrawerLayout getDrawerLayout()
	{
		return drawerLayout;
	}
}
