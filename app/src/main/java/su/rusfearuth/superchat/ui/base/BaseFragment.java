package su.rusfearuth.superchat.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public abstract class BaseFragment extends Fragment
{
	private ProgressDialog pleaseWaite;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		return inflater.inflate(getLayoutId(), null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		onBindListeners(view);
	}

	public void onBindListeners(@NonNull View view)
	{
	}

	public abstract int getLayoutId();

	public EventManager getEventManager()
	{
		return EventManager.getInstance();
	}

	public SuperChatManager getSuperChatManager()
	{
		return SuperChatManager.getInstance();
	}

	public void invalidateFonts()
	{
		if (isAdded() && getView() != null)
			TypefaceUtils.invalidateFonts(getActivity(), getView());
	}

	protected void showPleaseWaite()
	{
		dismissPleaseWaite();
		pleaseWaite = ProgressDialog.show(getActivity(), null,
				getString(R.string.dialog_message_please_waite), false, false);
	}

	protected void dismissPleaseWaite()
	{
		if (pleaseWaite != null && pleaseWaite.isShowing())
		{
			pleaseWaite.dismiss();
			pleaseWaite = null;
		}
	}
}
