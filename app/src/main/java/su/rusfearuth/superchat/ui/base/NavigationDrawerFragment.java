package su.rusfearuth.superchat.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.ui.about.AboutActivity;
import su.rusfearuth.superchat.ui.main.MainActivity;
import su.rusfearuth.superchat.ui.welcome.WelcomeActivity;
import su.rusfearuth.superchat.utils.TypefaceUtils;

public class NavigationDrawerFragment extends BaseFragment
	implements View.OnClickListener
{
	private ImageView avatar;
	private TextView name;

	@Override
	public int getLayoutId()
	{
		return R.layout.fragment_navigation_drawer;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		initView(view);
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onBindListeners(View view)
	{
		view.findViewById(R.id.btnChannel).setOnClickListener(this);
		view.findViewById(R.id.btnLogout).setOnClickListener(this);
		view.findViewById(R.id.btnAbout).setOnClickListener(this);
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.btnChannel:
				if (isAdded())
				{
					Intent intent = new Intent(getActivity(), MainActivity.class);
					Intent startIntent = IntentCompat.makeRestartActivityTask(intent.getComponent());
					startActivity(startIntent);
				}
				break;
			case R.id.btnAbout:
				if (isAdded())
				{
					Intent intent = new Intent(getActivity(), AboutActivity.class);
					Intent startIntent = IntentCompat.makeRestartActivityTask(intent.getComponent());
					startActivity(startIntent);
				}
				break;
			case R.id.btnLogout:
				if (isAdded())
				{
					getSuperChatManager().dropServerSettings();
					getEventManager().push(AppEvent.Server.CLOSE_CONNECTION, null);
					Intent intent = new Intent(getActivity(), WelcomeActivity.class);
					Intent startIntent = IntentCompat.makeRestartActivityTask(intent.getComponent());
					startActivity(startIntent);
				}
				break;
		}
	}

	private void initView(@NonNull View view)
	{
		name = (TextView) view.findViewById(R.id.lbName);
		name.setText(getSuperChatManager().getName());

		avatar = (ImageView) view.findViewById(R.id.imgAvatar);
		Picasso.with(getActivity())
				.load("https://avatars.githubusercontent.com/" + getSuperChatManager().getName())
				.into(avatar);
		if (isAdded())
			TypefaceUtils.invalidateFonts(getActivity(), view);
	}
}
