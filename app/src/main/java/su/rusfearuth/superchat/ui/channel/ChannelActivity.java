package su.rusfearuth.superchat.ui.channel;

import android.os.Bundle;

import su.rusfearuth.superchat.db.helper.ChannelHelper;
import su.rusfearuth.superchat.db.model.Channel;
import su.rusfearuth.superchat.ui.base.BaseBackActivity;
import su.rusfearuth.superchat.ui.channel.fragment.ChannelFragment;

import static su.rusfearuth.superchat.ui.channel.fragment.ChannelFragment.Params;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class ChannelActivity extends BaseBackActivity
{
	private Long channelId;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Channel channel = null;
		if (getIntent() != null)
		{
			channelId = getIntent().getLongExtra(Params.CHANNEL_ID, 0);
			channel = ChannelHelper.findById(channelId);
		}
		setTitle(channel.getName());
		super.onCreate(savedInstanceState);
		Bundle args = new Bundle();
		args.putLong(Params.CHANNEL_ID, channelId);

		addContentFragment(ChannelFragment.class, args, false);
	}

	@Override
	public void onSaveInstanceState(Bundle out)
	{
		out.putLong(Params.CHANNEL_ID, channelId);
		super.onSaveInstanceState(out);
	}

	@Override
	public void onRestoreInstanceState(Bundle in)
	{
		super.onRestoreInstanceState(in);
		channelId = in.getLong(Params.CHANNEL_ID);
	}
}
