package su.rusfearuth.superchat.ui.channel.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import rx.Subscription;
import rx.functions.Action1;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.db.CursorLoaderUtils;
import su.rusfearuth.superchat.db.helper.ChannelHelper;
import su.rusfearuth.superchat.db.helper.MessageHelper;
import su.rusfearuth.superchat.db.model.Channel;
import su.rusfearuth.superchat.db.model.Message;
import su.rusfearuth.superchat.ui.base.BaseFragment;
import su.rusfearuth.superchat.utils.MqttUtils;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class ChannelFragment extends BaseFragment
	implements View.OnClickListener
{
	private static final String LOG_TAG = ChannelFragment.class.getCanonicalName();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss / dd.MM");

	public interface Params
	{
		public static final String CHANNEL_ID = new StringBuilder(LOG_TAG)
				.append(".params.channel.id").toString();
	}

	private EditText message;
	private ListView list;
	private MessageAdapter adapter;
	private Channel channel;
	private Subscription sendMessageSub;

	@Override
	public int getLayoutId()
	{
		return R.layout.fragment_channel;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (getArguments() != null)
		{
			long id = getArguments().getLong(Params.CHANNEL_ID);
			channel = ChannelHelper.findById(id);
			Log.e("CHANNEL NAME", String.valueOf(channel.getName()));
		}
		if (channel != null)
		{
			getEventManager().push(AppEvent.Server.SUBSCRIBE_CHANNEL, channel.getName());
		}
		sendMessageSub = getEventManager().watch(AppEvent.Server.MESSAGE_ARRIVED)
				.subscribe(new Action1<Message>()
				{
					@Override
					public void call(Message o)
					{
						loadMessages();
						if (!isResumed() && o != null)
						{
							getEventManager().push(AppEvent.Server.NOTIFY_ABOUT_MESSAGE, o);
						}
						Log.e("INCOMMING MESSAGE", ">>> " + MessageHelper.count());
					}
				});
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		message = (EditText) view.findViewById(R.id.fldMessage);
		list = (ListView) view.findViewById(R.id.messages);
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onBindListeners(@NonNull View view)
	{
		view.findViewById(R.id.btnSend).setOnClickListener(this);
		view.findViewById(R.id.fldMessage).setOnClickListener(this);
//		ViewObservable.text(message, false).subscribe(new Action1<OnTextChangeEvent>()
//		{
//			@Override
//			public void call(OnTextChangeEvent onTextChangeEvent)
//			{
//				if (channel != null)
//					getSuperChatManager().sendTyping(channel.getName());
//			}
//		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		loadMessages();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btnSend:
				if (!TextUtils.isEmpty(message.getText().toString()))
				{
					Message localMessage = new Message();
					localMessage.setChannel(MqttUtils.fixChannelName(channel.getName()));
					localMessage.setName(getSuperChatManager().getName());
					localMessage.setMessage(message.getText().toString());
					localMessage.setForeigner(false);
					localMessage.setSent(false);
					localMessage.setDate(System.currentTimeMillis());
					localMessage.setUsername(getSuperChatManager().getName());
					localMessage.save();

					getEventManager().push(AppEvent.Server.SEND_MESSAGE, localMessage);

					message.setText("");
					message.setHint(R.string.message_hint_message);

					loadMessages();
				}
				break;
			case R.id.fldMessage:
				if (message.isFocused())
					new Handler().postDelayed(new Runnable()
					{
						@Override
						public void run()
						{
							list.smoothScrollToPosition(adapter.getCount() - 1);
						}
					}, 350);
				break;
		}
	}

	@Override
	public void onDestroy()
	{
		if (adapter != null && adapter.getCursor() != null && !adapter.getCursor().isClosed())
		{
			adapter.getCursor().close();
		}
		if (channel != null)
			getEventManager().push(AppEvent.Server.UNSUBSCRIBE_CHANNEL, channel.getName());
		if (sendMessageSub != null && !sendMessageSub.isUnsubscribed())
		{
			sendMessageSub.unsubscribe();
			sendMessageSub = null;
		}
		super.onDestroy();
	}

	private void loadMessages()
	{
		CursorLoaderUtils.loadMessage(channel.getName())
				.subscribe(new Action1<Cursor>()
				{
					@Override
					public void call(Cursor cursor)
					{
						if (cursor != null)
						{
							if (adapter == null)
							{
								adapter = new MessageAdapter(cursor);
								list.setAdapter(adapter);
								list.setSelection(adapter.getCount() - 1);
							}
							else
							{
								adapter.swapCursor(cursor);
								adapter.notifyDataSetChanged();
								list.smoothScrollToPosition(adapter.getCount() - 1);
							}
						}
					}
				});
	}

	private class ItemHolder
	{
		public TextView author;
		public TextView message;
		public TextView date;

		public ItemHolder(@NonNull View view)
		{
			author = (TextView) view.findViewById(R.id.lbAuthor);
			message = (TextView) view.findViewById(R.id.lbMessage);
			date = (TextView) view.findViewById(R.id.lbDate);
		}
	}

	private class MessageAdapter extends CursorAdapter
	{

		public MessageAdapter(Cursor c)
		{
			super(getActivity(), c, false);
		}

		@Override
		public int getViewTypeCount()
		{
			return 3;
		}

		@Override
		public int getItemViewType(int position)
		{
			int result = 0;
			Cursor cursor = (Cursor) getItem(position);
			if (cursor != null)
			{
				Message message = new Message();
				message.loadFromCursor(cursor);

				if (!message.getForeigner())
				{
					if (message.getSent())
					{
						result = 2;
					}
					else
					{
						result = 1;
					}
				}
				else
				{
					result = 0;
				}
			}
			return result;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent)
		{
			View view = LayoutInflater.from(context).inflate(R.layout.item_message, null);
			ItemHolder holder = new ItemHolder(view);
			view.setTag(holder);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor)
		{
			Message message = new Message();
			message.loadFromCursor(cursor);
			ItemHolder holder = (ItemHolder) view.getTag();

			holder.author.setText(message.getName());
			holder.message.setText(message.getMessage());


			int position = getCursor().getPosition();
			int itemType = getItemViewType(position);

			if (itemType == 1 || itemType == 2)
			{
				((LinearLayout) view).setGravity(Gravity.RIGHT);
			}
			else
			{
				((LinearLayout) view).setGravity(Gravity.LEFT);
			}

			if (itemType == 0 || itemType == 2)
			{
				holder.date.setText(dateFormat.format(message.getDate()));
			}

			if (isAdded())
				TypefaceUtils.invalidateFonts(getActivity(), view);
		}
	}

}
