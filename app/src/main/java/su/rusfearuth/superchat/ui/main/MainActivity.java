package su.rusfearuth.superchat.ui.main;

import android.os.Bundle;

import rx.Subscription;
import rx.functions.Action1;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.ui.base.BaseDrawerActivity;
import su.rusfearuth.superchat.ui.main.fragment.MainFragment;


public class MainActivity extends BaseDrawerActivity
{
	private Subscription channelOpenSub;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addContentFragment(MainFragment.class, null, false);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		channelOpenSub = getEventManager().watch(AppEvent.Interface.SELECT_CHANNEL)
				.subscribe(new Action1<Long>()
				{
					@Override
					public void call(Long id)
					{
						getDrawerLayout().closeDrawers();
					}
				});
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if (channelOpenSub != null && !channelOpenSub.isUnsubscribed())
		{
			channelOpenSub.unsubscribe();
			channelOpenSub = null;
		}
	}
}
