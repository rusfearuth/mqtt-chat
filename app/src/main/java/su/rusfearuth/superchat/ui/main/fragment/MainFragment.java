package su.rusfearuth.superchat.ui.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.db.CursorLoaderUtils;
import su.rusfearuth.superchat.db.helper.ChannelHelper;
import su.rusfearuth.superchat.db.model.Channel;
import su.rusfearuth.superchat.ui.base.BaseFragment;
import su.rusfearuth.superchat.ui.channel.ChannelActivity;
import su.rusfearuth.superchat.ui.channel.fragment.ChannelFragment;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 20.12.14.
 */
public class MainFragment extends BaseFragment
	implements View.OnClickListener, AdapterView.OnItemClickListener
{
	private EditText name;
	private Subscription channelOpenSub;
	private ChannelAdapter adapter;
	private View noChannels;
	private ListView channels;
	private Subscription channelSub;

	@Override
	public int getLayoutId()
	{
		return R.layout.fragment_main;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		initView(view);
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onBindListeners(@NonNull View view)
	{
		view.findViewById(R.id.btnOk).setOnClickListener(this);
		channels.setOnItemClickListener(this);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		channelOpenSub = getEventManager().watch(AppEvent.Interface.SELECT_CHANNEL)
				.delay(500, TimeUnit.MILLISECONDS, Schedulers.io())
				.subscribe(new Action1<Long>()
				{
					@Override
					public void call(Long id)
					{
						startChannel(id);
					}
				});
		channelSub = CursorLoaderUtils.loadChannels()
				.subscribe(new Action1<Cursor>()
				{
					@Override
					public void call(Cursor cursor)
					{
						if (cursor != null)
						{
							if (adapter == null)
							{
								adapter = new ChannelAdapter(cursor);
								channels.setAdapter(adapter);
							}
							else
							{
								adapter.swapCursor(cursor);
								adapter.notifyDataSetChanged();
							}
							if (adapter.getCount() > 0)
							{
								channels.setVisibility(View.VISIBLE);
								noChannels.setVisibility(View.GONE);
							}
							else
							{
								channels.setVisibility(View.GONE);
								noChannels.setVisibility(View.VISIBLE);
							}
						}
					}
				});
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if (channelOpenSub != null && !channelOpenSub.isUnsubscribed())
		{
			channelOpenSub.unsubscribe();
			channelOpenSub = null;
		}
		if (channelSub != null && !channelSub.isUnsubscribed())
		{
			channelSub.unsubscribe();
			channelSub = null;
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.btnOk:
				if (validate())
				{
					Channel channel = ChannelHelper.addChannel(name.getText().toString(), getSuperChatManager().getName());
					view.setEnabled(true);
					startChannel(channel.getId());
				}
				else
				{
					view.setEnabled(true);
					Toast.makeText(getActivity(), R.string.main_toast_error, Toast.LENGTH_LONG)
							.show();
				}
				break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		getEventManager().push(AppEvent.Interface.SELECT_CHANNEL, id);
	}

	@Override
	public void onDestroy()
	{
		if (adapter != null && adapter.getCursor() != null && !adapter.getCursor().isClosed())
		{
			adapter.getCursor().close();
		}
		super.onDestroy();
	}

	private void initView(@NonNull View view)
	{
		name = (EditText) view.findViewById(R.id.fldChannelName);
		noChannels = view.findViewById(R.id.lbNoChannels);
		channels = (ListView) view.findViewById(R.id.channels);
		if (isAdded())
			TypefaceUtils.invalidateFonts(getActivity(), view);
	}

	private void startChannel(@NonNull Long id)
	{
		Intent intent = new Intent(getActivity(), ChannelActivity.class);
		intent.putExtra(ChannelFragment.Params.CHANNEL_ID, id);
		startActivity(intent);
	}

	private boolean validate()
	{
		boolean result = true;

		String nameSource = name.getText().toString();

		if (TextUtils.isEmpty(nameSource))
			result = false;

		return result;
	}

	private class ItemHolder
	{
		private TextView name;
		private TextView count;

		public ItemHolder(@NonNull View view)
		{
			name = (TextView) view.findViewById(R.id.lbChannelName);
			count = (TextView) view.findViewById(R.id.lbChannelCount);
		}
	}

	private class ChannelAdapter extends CursorAdapter
	{

		public ChannelAdapter(Cursor c)
		{
			super(getActivity(), c, false);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent)
		{
			View view = LayoutInflater.from(context).inflate(R.layout.item_channel, null);
			ItemHolder holder = new ItemHolder(view);
			view.setTag(holder);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor)
		{
			Channel channel = new Channel();
			channel.loadFromCursor(cursor);

			ItemHolder holder = (ItemHolder) view.getTag();

			holder.name.setText(channel.getName());
			String username = getSuperChatManager().getName();
			int count = ChannelHelper.countMessages(channel.getName(), username);
			holder.count.setText(String.valueOf(count));

			if (isAdded())
				TypefaceUtils.invalidateFonts(getActivity(), view);
		}
	}
}
