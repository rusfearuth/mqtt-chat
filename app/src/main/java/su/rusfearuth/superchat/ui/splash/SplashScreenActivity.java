package su.rusfearuth.superchat.ui.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import rx.Subscription;
import rx.functions.Action1;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.ui.main.MainActivity;
import su.rusfearuth.superchat.ui.welcome.WelcomeActivity;
import su.rusfearuth.superchat.utils.TypefaceUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class SplashScreenActivity extends Activity
{
	private AtomicBoolean paused = new AtomicBoolean(false);
	private Subscription connectionOpenedSub;
	private Subscription connectionFailedSub;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		TypefaceUtils.invalidateFonts(this, getWindow().getDecorView());
		connectionOpenedSub = EventManager.getInstance().watch(AppEvent.Server.CONNECTION_OPENED).
				subscribe(new Action1()
				{
					@Override
					public void call(Object o)
					{
						Log.e("RECONNECT SUCCESS", String.valueOf(o));
						Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
						startActivity(intent);
					}
				});
		connectionFailedSub = EventManager.getInstance().watch(AppEvent.Server.CONNECTION_FAILED)
				.subscribe(new Action1<Throwable>()
				{
					@Override
					public void call(Throwable o)
					{
						Log.e("RECONNECT ERROR", String.valueOf(o));
					}
				});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		paused.set(false);
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				if (!paused.get())
				{
					Intent intent = null;
					if (SuperChatManager.getInstance().isAuth())
					{
						EventManager.getInstance().push(AppEvent.Server.OPEN_CONNECTION, null);
					}
					else
					{
						intent = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
					}
					if (intent != null)
						startActivity(intent);
				}
			}
		}, TimeUnit.SECONDS.toMillis(2));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		paused.set(true);
	}

	@Override
	public void onStop()
	{
		super.onStop();
		if (connectionOpenedSub != null && !connectionOpenedSub.isUnsubscribed())
		{
			connectionOpenedSub.unsubscribe();
			connectionOpenedSub = null;
		}
		if (connectionFailedSub != null && !connectionFailedSub.isUnsubscribed())
		{
			connectionFailedSub.unsubscribe();
			connectionFailedSub = null;
		}
	}
}
