package su.rusfearuth.superchat.ui.welcome;

import android.os.Bundle;

import su.rusfearuth.superchat.ui.base.BaseActivity;
import su.rusfearuth.superchat.ui.welcome.fragment.WelcomeFragment;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class WelcomeActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addContentFragment(WelcomeFragment.class, null, false);
	}
}
