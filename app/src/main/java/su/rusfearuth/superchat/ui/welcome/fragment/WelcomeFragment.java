package su.rusfearuth.superchat.ui.welcome.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import rx.Subscription;
import rx.functions.Action1;
import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.R;
import su.rusfearuth.superchat.event.EventManager;
import su.rusfearuth.superchat.ui.base.BaseFragment;
import su.rusfearuth.superchat.ui.main.MainActivity;
import su.rusfearuth.superchat.utils.MqttUtils;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class WelcomeFragment extends BaseFragment
	implements View.OnClickListener
{
	private EditText serverAddress;
	private EditText serverPort;
	private EditText name;
	private Subscription connectionOpenedSub;
	private Subscription connectionFailedSub;

	@Override
	public int getLayoutId()
	{
		return R.layout.fragment_welcome;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		connectionOpenedSub = EventManager.getInstance().watch(AppEvent.Server.CONNECTION_OPENED).
				subscribe(new Action1()
				{
					@Override
					public void call(Object o)
					{
						dismissPleaseWaite();
						Log.e("RECONNECT SUCCESS", String.valueOf(o));
						Intent intent = new Intent(getActivity(), MainActivity.class);
						Intent startIntent = IntentCompat.makeRestartActivityTask(intent.getComponent());
						startActivity(startIntent);
					}
				});
		connectionFailedSub = EventManager.getInstance().watch(AppEvent.Server.CONNECTION_FAILED)
				.subscribe(new Action1<Throwable>()
				{
					@Override
					public void call(Throwable o)
					{
						Log.e("RECONNECT ERROR", String.valueOf(o));
						dismissPleaseWaite();
						if (isAdded())
							Toast.makeText(getActivity(), o.getMessage(), Toast.LENGTH_LONG).show();
						if (isAdded() && getView() != null)
						{
							getView().findViewById(R.id.btnContinue).setEnabled(true);
						}
					}
				});
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		initView(view);
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onBindListeners(@NonNull View view)
	{
		view.findViewById(R.id.btnContinue).setOnClickListener(this);
	}

	@Override
	public void onClick(View view)
	{
		view.setEnabled(false);
		switch (view.getId())
		{
			case R.id.btnContinue:
				int message = validate();
				if (message == 0)
				{
					getSuperChatManager().setName(name.getText().toString());

					getSuperChatManager().setServerClientId(MqttUtils.generateClientId());
					getSuperChatManager().setServerAddress(serverAddress.getText().toString());
					getSuperChatManager().setServerPort(Integer.valueOf(serverPort.getText().toString()));
					showPleaseWaite();
					getEventManager().push(AppEvent.Server.OPEN_CONNECTION, null);
				}
				else
				{
					view.setEnabled(true);
					Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
							.show();
				}
				break;
		}
	}

	@Override
	public void onDestroy()
	{
		if (connectionOpenedSub != null && !connectionOpenedSub.isUnsubscribed())
		{
			connectionOpenedSub.unsubscribe();
			connectionOpenedSub = null;
		}
		if (connectionFailedSub != null && !connectionFailedSub.isUnsubscribed())
		{
			connectionFailedSub.unsubscribe();
			connectionFailedSub = null;
		}
		super.onDestroy();
	}

	private void initView(@NonNull View view)
	{
		name = (EditText) view.findViewById(R.id.fldName);
		serverAddress = (EditText) view.findViewById(R.id.fldServerAddress);
		serverPort = (EditText) view.findViewById(R.id.fldServerPort);
		invalidateFonts();
	}

	private int validate()
	{
		int result = 0;

		String nameSource = name.getText().toString();
		String serverAddressSource = serverAddress.getText().toString();
		String serverPortSource = serverPort.getText().toString();


		if (TextUtils.isEmpty(serverAddressSource))
			return R.string.welcome_toast_empty_address;

		if (!Patterns.IP_ADDRESS.matcher(serverAddressSource).matches())
			return R.string.welcome_toast_invalid_address;

		if (TextUtils.isEmpty(serverPortSource))
			return R.string.welcome_toast_empty_port;

		if (!TextUtils.isDigitsOnly(serverPortSource))
			return R.string.welcome_toast_invalid_port;

		if (TextUtils.isEmpty(nameSource))
			return R.string.welcome_toast_error;

		return result;
	}
}
