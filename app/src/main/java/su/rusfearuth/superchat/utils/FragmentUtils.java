package su.rusfearuth.superchat.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class FragmentUtils
{
	public static void addFragment(FragmentActivity activity, int id, Class<? extends Fragment> clazz,
	                               Bundle args, boolean toStack)
	{
		if (activity != null)
		{
			FragmentManager manager = activity.getSupportFragmentManager();
			FragmentTransaction tx = manager.beginTransaction();
			if (toStack)
			{
				tx.addToBackStack(null);
			}
			else
			{
				Fragment previous = manager.findFragmentById(id);
				if (previous != null)
					tx.remove(previous);
			}
			tx.add(id, Fragment.instantiate(activity, clazz.getName(), args));
			tx.commit();
		}
	}
}
