package su.rusfearuth.superchat.utils;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import su.rusfearuth.superchat.AppEvent;
import su.rusfearuth.superchat.context.SuperChatManager;
import su.rusfearuth.superchat.db.helper.MessageHelper;
import su.rusfearuth.superchat.db.model.Message;
import su.rusfearuth.superchat.event.EventManager;

/**
 * Created by rusfearuth on 21.12.14.
 */
public class MqttUtils
{
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

	public static String fixChannelName(@NonNull String name)
	{
		String result = name;
		if (!name.startsWith("/"))
			result = new StringBuilder("/").append(name).toString();
		return result;
	}

	public static void processMessage(@NonNull String channel, @NonNull JSONObject msg, @NonNull String myClientId)
	{
		String username = SuperChatManager.getInstance().getName();
		if (msg.has("_lid"))
		{
			String clientId = msg.optString("clientId");
			if (myClientId.equals(clientId))
			{
				Message local = MessageHelper.findById(msg.optLong("_lid"));
				local.setSent(true);
				local.save();
				EventManager.getInstance().push(AppEvent.Server.MESSAGE_ARRIVED, null);
			}
			else
			{
				Message message = MessageHelper.addNewMessage(msg, fixChannelName(channel), username);
				EventManager.getInstance().push(AppEvent.Server.MESSAGE_ARRIVED, message);
			}
		}
		else
		{
			Message message = MessageHelper.addNewMessage(msg, fixChannelName(channel), username);
			EventManager.getInstance().push(AppEvent.Server.MESSAGE_ARRIVED, message);
		}
	}

	public static String generateClientId()
	{
		long max = 999999998l;
		long min = 666666667l;
		long clientId = (long) (Math.random() * (max - min) + min);
		return String.valueOf(clientId);
	}

	public static String currentDate()
	{
		return dateFormat.format(new Date(System.currentTimeMillis()));
	}
}
