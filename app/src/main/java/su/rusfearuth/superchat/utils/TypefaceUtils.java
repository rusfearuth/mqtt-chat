package su.rusfearuth.superchat.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

/**
 * Created by rusfearuth on 19.12.14.
 */
public class TypefaceUtils
{
	// cache reduce time for more 5x
	private static final LruCache<String, Typeface> fontCache = new LruCache<>(2 * 1024 * 1024);
	private static String defaultFont;

	public static void setDefaultFont(@NonNull String font)
	{
		defaultFont = font;
	}

	public static void invalidateFonts(@NonNull Context context, @NonNull View view)
	{
		AssetManager manager = null;
		if (context != null)
			manager = context.getAssets();


		if (manager == null)
			return;


		ViewGroup container = (ViewGroup) view;
		for (int i = 0; i < container.getChildCount(); i++)
		{
			View child = container.getChildAt(i);


			if (child instanceof ViewGroup)
			{
				invalidateFonts(context, child);
			}
			else if (child instanceof TextView)
			{
				replaceFonts(manager, child);
			}
		}
	}


	public static void invalidateFonts(@NonNull Context context, @NonNull View view, @NonNull String font)
	{
		AssetManager manager = context.getAssets();


		if (manager == null)
			return;


		if (view instanceof ViewGroup)
		{
			ViewGroup container = (ViewGroup) view;
			for (int i = 0; i < container.getChildCount(); i++)
			{
				View child = container.getChildAt(i);


				if (child instanceof ViewGroup)
				{
					invalidateFonts(context, child, font);
				}
				else if (child instanceof TextView)
				{
					String viewFont = (String) child.getTag();
					if (TextUtils.isEmpty(viewFont))
						child.setTag(font);
					replaceFonts(manager, child);
				}
			}
		}
		else
		{
			String viewFont = (String) view.getTag();
			if (TextUtils.isEmpty(viewFont))
				view.setTag(font);
			replaceFonts(manager, view);
		}
	}

	public static void replaceFonts(@NonNull AssetManager assertManager, @NonNull View view)
	{
		String tag = (String) view.getTag();
		if (!TextUtils.isEmpty(tag))
		{
			Typeface typeface = fontCache.get(tag);
			if (typeface == null)
			{
				String typefacePath = new StringBuilder("fonts").append(File.separator).append(tag).toString();
				typeface = Typeface.createFromAsset(assertManager, typefacePath);
				fontCache.put(tag, typeface);
			}
			((TextView) view).setTypeface(typeface);
			if (view instanceof Button && Build.VERSION.SDK_INT < 14)
				((Button) view).setText(((Button) view).getText().toString().toUpperCase());
		}
		else
		{
			if (TextUtils.isEmpty(defaultFont))
				throw new NullPointerException("Default font is missing for TypefaceUtils!");
			view.setTag(defaultFont);
			replaceFonts(assertManager, view);
		}
	}
}
