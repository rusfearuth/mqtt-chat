package su.rusfearuth.superchat.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by rusfearuth on 22.12.14.
 */
public class UiUtils
{
	public static void hideSoftwareKeyboard(Context context)
	{
		if (context != null)
		{
			InputMethodManager manager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
			manager.hideSoftInputFromWindow(null, 0);
		}
	}
}
